import React from 'react'
import {getLogin} from '../../helpers/getLogin';
import useForm from '../../hooks/useForm';

import './login.css';

const Login = ({history}) => {

    const [credenciales, handleInputchange] = useForm({
        email: '',
        password: ''
    });
    const {email, password} = credenciales;

    const handleLogin = (e) => {
        e.preventDefault();
        getLogin(credenciales).then(user => {
            console.log(user);
            localStorage.setItem('Session', JSON.stringify(user));

            history.replace('/welcomeClientes');
            
        });
    }

    return (
        <form id="LoginForm">
            <div className="row col-sm-6 form-group">
                <label htmlFor="email"> Correo </label>
                <input
                    type="text"
                    name="email"
                    className="form-control"
                    value={email}
                    onChange={handleInputchange}
                />
            </div>

            <div className="row col-sm-6 form-group">
                <label htmlFor="password"> Contraseña </label>
                <input
                    type="password"
                    name="password"
                    className="form-control"
                    value={password}
                    onChange={handleInputchange}
                />
            </div>

            <hr/>

            <button className="btn btn-primary" type="submit" onClick={handleLogin}>Ingresar</button>

        </form>
    )
}

export default Login;   