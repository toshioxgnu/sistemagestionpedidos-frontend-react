import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { getProductos } from '../../helpers/getProductos';
import ProductoItem from '../productos/ProductoItem';

const PedidoScreen = () => {

    const { id_usuario } = useParams();

    const [listaproductos, setlistaproductos] = useState([]);

    useEffect(() => {
        getProductos(id_usuario).then(
            productlist => {setlistaproductos(productlist)}
        )
    }, [id_usuario]);

    return (
        <div className="container-fluid col-md-6"
            style={{
                padding: 10
            }}
        >
            <div className="col-md-4 col-xs-4">
                {
                    listaproductos.map(
                        producto => (
                            <ProductoItem 
                                key={producto.id_producto}
                                {...producto}
                            />
                        )
                    )
                }
            </div>
            
        </div>
    )
}

export default PedidoScreen
