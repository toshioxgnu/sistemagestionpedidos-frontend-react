import React, {useState} from 'react'
import {insertUsuario} from '../../helpers/insertUsuario';
import useForm from '../../hooks/useForm'

const Register = () => {

    const [tipocliente, settipocliente] = useState('selected')

    const [cliente, handleInputchange] = useForm({
        nombre: '',
        apellido: '',
        email: '',
        password: '',
        nombreusuario: '',
        direccion: '',
        region: '',
        comuna: ''
    });

    const {
        nombre,
        apellido,
        email,
        password,
        nombreusuario,
        direccion,
        region,
        comuna
    } = cliente;

    const handleSubmitForm = (e) => {
        e.preventDefault()
        insertUsuario(cliente, tipocliente)
            .then(newuser => {
                console.log(newuser);
            })
    }

    return (
        <div className="col-md-6">
            <h2> Datos : </h2>

            <form onSubmit={handleSubmitForm}>
                <table>
                    <tbody>
                    <tr>
                        <td><label htmlFor="tipocliente">Registrarse como: </label></td>
                        <td>
                            <select id="tipocliente" name='tipocliente' onChange={(e) => {
                                settipocliente(e.target.value)
                            }} className='form-select form-select-sm'>
                                <option value={tipocliente}>Seleccione Tipo</option>
                                <option value="clientes">Cliente</option>
                                <option value="usuarios">Negocio</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Nombre:</td>
                        <td><input value={nombre} name="nombre" type="text" onChange={handleInputchange}
                                   className="form-control" required/></td>
                    </tr>
                    <tr>
                        <td>Apellido:</td>
                        <td><input value={apellido} name="apellido" type="text" onChange={handleInputchange}
                                   className="form-control" required/></td>
                    </tr>
                    <tr>
                        <td>Nombre Usuario:</td>
                        <td><input value={nombreusuario} name="nombreusuario" onChange={handleInputchange} type="text"
                                   className="form-control" required/></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><input value={email} name="email" onChange={handleInputchange} type="text"
                                   className="form-control" required/></td>
                    </tr>
                    <tr>
                        <td>Contraseña:</td>
                        <td><input value={password} name="password" onChange={handleInputchange} type="password"
                                   className="form-control" required/></td>
                    </tr>
                    </tbody>
                </table>
                {
                    (tipocliente === 'clientes') && (
                        <div>
                            <h3>Datos de envio:</h3>
                            <table>
                                <tbody>
                                <tr>
                                    <td>Direccion:</td>
                                    <td><input placeholder="Direccion #1111" name="direccion"
                                               onChange={handleInputchange} type="text" value={direccion}
                                               className="form-control"/></td>
                                </tr>
                                <tr>
                                    <td>Region:</td>
                                    <td><input type="text" name="region" value={region} onChange={handleInputchange}
                                               className="form-control"/></td>
                                </tr>
                                <tr>
                                    <td>Comuna:</td>
                                    <td><input type="text" name="comuna" value={comuna} onChange={handleInputchange}
                                               className="form-control"/></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    )
                }

                <button className="btn btn-info" type="submit">Registrar</button>
            </form>
            <hr/>
        </div>
    )
}

export default Register
