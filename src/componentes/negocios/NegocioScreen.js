import React, { useEffect, useState } from 'react'
import getUsuarioingreso from '../../helpers/getUsuarioingreso';
import ProductosList from '../productos/ProductosList'

const NegocioScreen = () => {

    const [usuario, setusuario] = useState({
        id_usuario: ''
    });

    useEffect(() => {
        const usuarioIngreso = getUsuarioingreso();
        setusuario(usuarioIngreso);
    }, [])
    
    const {id_usuario } = usuario;
    return (
        <div className="col-md-6 offset-1">
            <ProductosList id_usuario={id_usuario} />
        </div>
    )
}

export default NegocioScreen
