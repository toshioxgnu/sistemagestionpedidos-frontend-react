import React, { useEffect, useState } from 'react'
import getUsuarioingreso from '../../helpers/getUsuarioingreso';
import insertProducto from '../../helpers/insertProducto';
import useForm from '../../hooks/useForm'

const CreaProductos = () => {

    const [usuario, setusuario] = useState({});

    useEffect(() => {
        const usuarioIngreso = getUsuarioingreso();

        setusuario(usuarioIngreso);

        
    }, []);

    const { id_usuario } = usuario;

    const [ producto, handleInputchange ] = useForm({
        cod_producto: '',
        nombre_producto: '',
        marca_producto: '',
        valor: ''
    });

    const {
        cod_producto,
        nombre_producto,
        marca_producto,
        valor,
    } = producto


    const handleFormSubmit = (e) => {
        e.preventDefault();
        insertProducto(producto, id_usuario).then(
            newproducto => {
                console.log(newproducto)
            }
        )
    }
    

    return (
        <div className="col-md-6 offset-1" 
            style={
                {
                    paddingTop: 5
                }
            }
        >
            <h3>Datos Producto:</h3>

            <form onSubmit={ handleFormSubmit }>            
                <table 
                    style={
                        {
                            paddingTop: 10
                        }
                    }
                >
                <tbody>
                    <tr>
                            
                            <td>Cod. Producto: </td>
                            <td><input  
                                type="text"
                                placeholder="Si amerita"
                                name="cod_producto"
                                value={ cod_producto }
                                onChange={handleInputchange}
                            /></td>
                        </tr>
                        <tr>
                            <td>Nombre Producto: </td>
                            <td><input  
                                type="text"
                                name="nombre_producto"
                                value={ nombre_producto }
                                onChange={ handleInputchange }
                            /></td>
                        </tr>
                        <tr>
                            <td>Marca Producto: </td>
                            <td><input  
                                type="text"
                                name="marca_producto"
                                value={ marca_producto }
                                onChange={ handleInputchange }
                            /></td>
                        </tr>
                        <tr>
                            <td>Valor Producto ($) : </td>
                            <td><input  
                                type="text"
                                name="valor"
                                value={ valor }
                                onChange={ handleInputchange }
                            /></td>
                        </tr>
                    </tbody>                    
                </table>

                <div className="row col-md-3">
                    <button type="submit" className="btn btn-info">Crear Producto</button>    
                </div>
            </form>

        </div>
    )
}

export default CreaProductos
