import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import {getProductos} from '../../helpers/getProductos';
import ProductoItem from './ProductoItem';

const ProductosList = ({id_usuario}) => {

    const [productlist, setproductlist] = useState([]);

    useEffect(() => {
        getProductos(id_usuario)
        .then(
            listaproductos => {
                setproductlist(listaproductos);
            }
        )
    }, [id_usuario])

    console.log(productlist)

    return (
        <div className="container-fluid col-md-6">
            <div className="row">
                <Link to="/creaProducto">
                    <button className="btn btn-success">Agregar Producto</button>
                </Link>                
            </div>

            <div className="row"
                style={
                    {
                        paddingTop: 20
                    }
                }
            >
                <div className="col-md-8">
                    {
                        productlist.map(
                            producto => (
                                <ProductoItem  
                                key={ producto.id_producto }
                                {...producto} />
                            )
                        )
                    }
                </div>
            </div>
        </div>
    )
}

export default ProductosList
