import React from 'react'

const ProductoItem = ({
    id_producto,
    nombre_producto,
    marca_producto,
    cod_producto,
    valor
}) => {
    

    return (
        <div className="card" key={id_producto}>
            <div className="card-header">
                { nombre_producto } <small><b> {marca_producto} </b></small>
            </div>
            <div className="card-body">
                <ul>
                    <li> Codigo Producto:  {cod_producto }</li>
                    <li> Valor: $ {valor} </li>
                </ul>
            </div>
        </div>
    )
}

export default ProductoItem
