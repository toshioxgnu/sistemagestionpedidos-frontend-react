import React, {useEffect, useState} from 'react'
import {getNegocios} from '../../helpers/getNegocios'
import NegociosItem from './NegociosItem';

const NegociosList = () => {

    const [negocioslist, setnegocioslist] = useState([])

    useEffect(() => {
        getNegocios().then(
            negocios => {
                setnegocioslist(negocios)
            }
        );
    }, [])


    console.log(negocioslist)
    return (
        <div className="container-fluid col-sm-6 mt-5">

            <h3>Negocios Cercanos</h3>
            <hr/>


            <table className="table">
                <thead className="thead-dark">
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Nombre Usuario</th>
                    <th>Accion</th>
                </tr>
                </thead>
                <tbody className="animate__animated  animate__fadeIn">
                {
                    negocioslist.map(
                        negocio => (
                            <NegociosItem
                                key={negocio.id_usuario}
                                {...negocio}
                            />
                        )
                    )

                }
                </tbody>
            </table>
        </div>
    )
}

export default NegociosList
