import React from 'react'
import { Link } from 'react-router-dom'

const NegociosItem = ({
                          id_usuario,
                          nombre,
                          apellido,
                          email,
                          nombreusuario,
                      }) => {


    return (
        <>
            <tr key={id_usuario}>
                <td>{nombre} {apellido} </td>
                <td> {email} </td>
                <td> {nombreusuario} </td>
                <td>
                    <Link to={`/pedidoScreen/${id_usuario}`}>
                        <button className="btn btn-info">Realizar pedido</button>
                    </Link>
                </td>
            </tr>
        </>
    )
}

export default NegociosItem
