import React from 'react';
import ReactDOM from 'react-dom';
import GestionPedidosApp from './GestionPedidosApp';
import './index.css';


ReactDOM.render(
    <GestionPedidosApp />,
  document.getElementById('root')
);
