import React from 'react'
import {AppRouter} from './routers/AppRouter'

const GestionPedidosApp = () => {
    return (
        <div>
            <AppRouter/>
        </div>
    )
}

export default GestionPedidosApp
