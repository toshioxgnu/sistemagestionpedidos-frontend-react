import React from 'react'
import {Link, NavLink} from 'react-router-dom'
import getUsuarioingreso from '../helpers/getUsuarioingreso';

export const NavBar = () => {

    const clearStorage = () => {
        localStorage.clear();
    }

    const usuario = getUsuarioingreso();

    return (
        <div>
            <nav className="navbar navbar-dark navbar-expand-sm bg-dark">
                <Link
                    className="navbar-brand"
                    to="/dashboard"
                > Gestion Pedidos </Link>
            
                {
                    (usuario.dtype ==='Cliente')&&(
                        <div className="navbar-collapse">
                            <div className="navbar-nav">

                                <NavLink
                                    activeClassName="active"
                                    className="nav-item nav-link"
                                    exact
                                    to="/welcomeClientes"
                                >
                                    Negocios
                                </NavLink>

                            </div>
                        </div>
                    )
                }
                {        
                    (usuario.dtype ==='Usuario')&&(
                        <div className="navbar-collapse">
                            <div className="navbar-nav">

                                <NavLink
                                    activeClassName="active"
                                    className="nav-item nav-link"
                                    exact
                                    to="/negocioScreen"
                                >
                                    Productos
                                </NavLink>

                            </div>
                        </div>
                    )
                }
                    
                <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
                    <ul className="navbar-nav ml-auto">
                        <NavLink
                            activeClassName="active"
                            className="nav-item nav-link"
                            exact
                            to="/login"
                            onClick={clearStorage}
                        >
                            Logout
                        </NavLink>
                    </ul>
                </div>

            </nav>
        </div>
    )
}
