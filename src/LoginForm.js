import React, {useState} from 'react'
import Login from './componentes/login/Login';
import Register from './componentes/register/Register';


import './LoginForm.css';

const LoginForm = ({history}) => {

    const [register, setregister] = useState(false);

    const handleRegistro = () => {
        setregister(!register);
    }

    return (
        <div className="container-fluid col-md-6 col-xs-6 col-lg-6 ">

            {
                register ? (
                        <div className="register">
                            <Register/>
                            <div className="retorna">
                                <button className="btn btn-info " onClick={handleRegistro}>Volver</button>
                            </div>

                        </div>
                    )
                    : (
                        <div className="login">
                            <Login history = {history} />
                            <p className="registerp"> No estas registrado?. <button className="btn btn-info btn-sm"
                                                             onClick={handleRegistro}>Registrar</button></p>
                        </div>

                    )
            }

        </div>
    )
}

export default LoginForm
