import {useState} from 'react';

const useForm = (initialState = {}) => {
    const [values, setstate] = useState(initialState);

    const handleInputchange = ({target}) => {
        setstate({
            ...values,
            [target.name]: target.value
        })
    }


    return [values, handleInputchange]

}

export default useForm
