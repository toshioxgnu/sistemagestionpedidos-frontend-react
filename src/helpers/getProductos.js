
export const getProductos = async( idusuario ) => {

    const requestOptions = {
        method: 'POST',
    }

    const url = `http://192.168.1.105:43000/api/productos/${idusuario}`;

    const resp = await fetch(url, requestOptions);
    const products = await resp.json();

    return products;
}

