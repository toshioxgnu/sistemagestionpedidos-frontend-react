export const insertUsuario = async (usuario, tipo) => {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(usuario)
    }
    const url = `http://192.168.1.105:43000/api/${tipo}/new`;

    const resp = await fetch(url, requestOptions);
    const newuser = await resp.json();

    return newuser;

}

