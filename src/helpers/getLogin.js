export const getLogin = async (credentials) => {

    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(credentials)
    }

    const url = 'http://192.168.1.105:43000/api/usuarios/login';
    const resp = await fetch(url, requestOptions);

    const user = await resp.json();
    return user;

}