export const getNegocios = async () => {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'}
    }
    const url = `http://192.168.1.105:43000/api/clientes/getNegocios`;

    const resp = await fetch(url, requestOptions);
    const negocios = await resp.json()

    return negocios;
}
