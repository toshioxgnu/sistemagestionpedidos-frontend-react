const insertProducto = async(producto, id_usuario) => {

    producto = {
        ...producto,
        id_usuario
    }
    console.log(producto)

    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(producto)
    }

    const url = `http://192.168.1.105:43000/api/productos/new`;

    const resp = await fetch(url, requestOptions);
    const newproducto = await resp.json();

    return newproducto;
}

export default insertProducto
