import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import ClientesScreen from '../componentes/clientes/ClientesScreen';
import LoginForm from '../LoginForm';
import { DashboardRouter } from './DashboardRouter';

export const AppRouter = () => {

    return (
        <Router>
            <div>
                <Switch>
                    <Route path="/clientes" component={ClientesScreen}/>
                    <Route exact path="/login" component={LoginForm}/>
                    <Route path="/" component={DashboardRouter}/>
                </Switch>

            </div>
            
        </Router>
)

}
