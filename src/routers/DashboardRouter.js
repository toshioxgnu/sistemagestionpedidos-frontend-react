import React, { Fragment } from 'react';
import {
    Switch,
    Route
} from "react-router-dom";
import ClientesScreen from '../componentes/clientes/ClientesScreen';
import NegocioScreen from '../componentes/negocios/NegocioScreen';
import PedidoScreen from '../componentes/pedidos/PedidoScreen';
import CreaProductos from '../componentes/productos/CreaProductos';
import {NavBar} from '../ui/NavBar';

export const DashboardRouter = () => {


    return (
        <Fragment>
            <NavBar/>
            <div>
                <Switch>
                        <Route  exact path="/negocioScreen" component={ NegocioScreen }  />                      
                        <Route exact path="/welcomeClientes" component={ClientesScreen}/>
                        <Route exact path="/pedidoScreen/:id_usuario" component={ PedidoScreen }/>
                        <Route exact path="/creaProducto" component={ CreaProductos }/>
                </Switch>
            </div>
        </Fragment>
    )

}
